// IHardwarePropertiesService.aidl
package com.technicolor.hardwareproperties;

import android.os.CpuUsageInfo;

interface IHardwarePropertiesService {
    float getCpuTemperature();
    CpuUsageInfo[] getCpuUsages();
}
