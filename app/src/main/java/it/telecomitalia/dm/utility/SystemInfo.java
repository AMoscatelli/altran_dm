package it.telecomitalia.dm.utility;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.Instrumentation;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.CpuUsageInfo;
import android.os.Environment;
import android.os.HardwarePropertiesManager;
import android.os.PowerManager;
import android.os.StatFs;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import it.telecomitalia.dm.models.Pinfo;

import static android.content.Context.ACTIVITY_SERVICE;


/**
 * Created by MW on 09/10/2017.
 */

public class SystemInfo {


    /**
     *
     * Get "key" from Constant class or get a key from properties
     * list from shell: getprop
     *
     * @param key
     *
     * @return Key value if exist or "unknown" if not present
     */

    public static String getSystemProperties(String key){
        //ro.serialno
        String serialNumber = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class );
            serialNumber = (String)(   get.invoke(c, key, "unknown" )  );
        }
        catch (Exception ignored)
        {
            return serialNumber;
        }
        return serialNumber;
    }


    /**
     *
     * @param activity
     * @return  true if STB is connected throught WiFi
     *          false if STB is connected throught ETH
     */

    public static Boolean isWifiConnected(Context activity){
        Boolean wifi = true;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(activity.getApplicationContext().CONNECTIVITY_SERVICE);
        try {
            if (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET)
                wifi = false;
        }catch(NullPointerException e){
            return false;
        }
        return wifi;
    }

    /**
     *
     * @param context
     * @return  true if a Google Account nhas been added into STB
     *
     */
    public static Boolean googleAccountPresent(Context context){
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccounts();

        if (accounts.length > 0) {
            return true;
        } else {
            return  false;
        }
    }

    /**
     *
     * @param context
     * @return  true if development Mode (Debug) is active
     *
     */
    public static Boolean developmentModeActive(Context context){

        if(Settings.Secure.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0)==0){
            return false;
        }else{
            return true;
        }

    }

    public static String getWifiSignal(Context activity) {

        int signal = 0;

        if(isWifiConnected(activity)) {
            ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            WifiManager wifiManager = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            if (mWifi.isConnected()) {
                int rssi = wifiManager.getConnectionInfo().getRssi();
                int level = WifiManager.calculateSignalLevel(rssi, 100);
                signal = (int) ((level));
            }

            return String.valueOf(signal) + "%";
        }else{
            return "";
        }
    }

    public static String readFreeMem(Context context){
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        return String.valueOf((mi.availMem / 1048576L));
    }

    public static String getUsedMemorySize() {


        long freeSize = 0L;
        long totalSize = 0L;
        long usedSize = -1L;
        try {
            Runtime info = Runtime.getRuntime();
            freeSize = info.freeMemory();
            totalSize = info.totalMemory();
            usedSize = totalSize - freeSize;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(usedSize);

    }

    /**
     * ONLY FOR A7 do not use on > A8
     * @return integer Array with 4 elements: user, system, idle and other cpu
     *         usage in percentage.
     */
    public static String getCpuUsageStatistic() {

        String tempString = executeTop();
        //String tempString = usage;

        /*tempString = tempString.replaceAll(",", "");
        tempString = tempString.replaceAll("User", "");
        tempString = tempString.replaceAll("System", "");
        tempString = tempString.replaceAll("IOW", "");
        tempString = tempString.replaceAll("IRQ", "");
        tempString = tempString.replaceAll("%", "");
        for (int i = 0; i < 10; i++) {
            tempString = tempString.replaceAll("  ", " ");
        }
        tempString = tempString.trim();
        String[] myString = tempString.split(" ");
        int[] cpuUsageAsInt = new int[myString.length];
        for (int i = 0; i < myString.length; i++) {
            myString[i] = myString[i].trim();
            cpuUsageAsInt[i] = Integer.parseInt(myString[i]);
        }
        return String.valueOf(cpuUsageAsInt[0]+cpuUsageAsInt[1]);*/
        return tempString;
    }

    /**
     * ONLY FOR A7 do not use on > A8
     * @return integer Array with 4 elements: user, system, idle and other cpu
     *         usage in percentage.
     */
    public static String CPU(Context ctx){

        HardwarePropertiesManager propertiesManager = (HardwarePropertiesManager)ctx.getSystemService(Context.HARDWARE_PROPERTIES_SERVICE);
        CpuUsageInfo[] cpuUsages = propertiesManager.getCpuUsages();
        return  cpuUsages[0].toString();
    }

    private static String executeTop() {
        java.lang.Process p = null;
        java.lang.Process p1 = null;
        BufferedReader in = null;
        String returnString = null;
        try {
            p = Runtime.getRuntime().exec("top -n 1");
            //p1 = Runtime.getRuntime().exec("cat /sdcard/appo.txt");//top -n 1
            in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            int read;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((read = in.read(buffer)) > 0) {
                output.append(buffer, 0, read);
            }
            String output1 = output.toString();
            String usr = output1.substring(output1.indexOf("user")-3,output1.indexOf("user")-1).trim();
            String sys = output1.substring(output1.indexOf("sys")-3,output1.indexOf("sys")-1).trim();
            returnString = String.valueOf(Integer.valueOf(usr)+ Integer.valueOf(sys));
            System.out.println(usr);
        } catch (IOException e) {
            Log.e("executeTop", "error in getting first line of top");
            e.printStackTrace();
        } finally {
            try {
                in.close();
                p.destroy();
            } catch (IOException e) {
                Log.e("executeTop",
                        "error in closing and destroying top process");
                e.printStackTrace();
            }
        }
        return returnString;
    }

    public static void executeKeyCode(int keyCode) {
        Instrumentation inst = new Instrumentation();
        inst.sendKeyDownUpSync(keyCode);

    }

    public static void executeReboot(Context ctx){

        try {
            PowerManager pm = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
            PackageManager pk = ctx.getPackageManager();
            pm.reboot(null);

        } catch (Exception ex) {
            Log.i("", "Could not reboot", ex);
        }
    }

    @Deprecated
    public static String getForegroundApp(Context mContext) throws PackageManager.NameNotFoundException {

        ActivityManager.RunningTaskInfo info = null;
        ActivityManager am;
        am = (ActivityManager)mContext.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> l = am.getRunningTasks(1000);
        System.out.println(l);
        Iterator<ActivityManager.RunningTaskInfo> i = l.iterator();


        String packName = new String();
        String appName = new String();
        ApplicationInfo appInfo = new ApplicationInfo();

        while(i.hasNext()){
            info = i.next();
            packName = info.topActivity.getPackageName();
            if(!packName.equals("com.htc.launcher") && !packName.equals("com.android.launcher")){ //this could be what is causing the problem. not sure.
                packName = info.topActivity.getPackageName();
                break;
            }
            info = i.next();
            packName= info.topActivity.getPackageName();
            break;
        }
        return packName;
    }

    /**
     * Used to retrice the foreground APK
     * usable unly from system app
     * @return
     */
    public static String executeTopApplication() {
        ArrayList<String> line = new ArrayList<>();

        String line1 = "";
        java.lang.Process p = null;
        BufferedReader in = null;
        int i=0;
        try {
            StringBuilder log=new StringBuilder();

            //p = Runtime.getRuntime().exec("top -n 1 -m 10 -s cpu");
            p = Runtime.getRuntime().exec("dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'");
            in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line1 = in.readLine()) != null) {
                if(line1.contains("mCurrentFocus")) {
                    Log.d("DM topApplication", line1);
                    line.add(i, line1);
                }

            }
        } catch (IOException e) {
            Log.e("executeTop", "error in getting first line of top");
            e.printStackTrace();
        } finally {
            try {
                in.close();
                p.destroy();
            } catch (Exception e) {
                Log.e("executeTop",
                        "error in closing and destroying top process");
                e.printStackTrace();
            }
        }
        String app;
        try {
             app = line.get(0).substring(line.get(0).lastIndexOf(" "), line.get(0).lastIndexOf("/"));
        }catch (StringIndexOutOfBoundsException e){
             app = "timvision.launcher";
        } catch (IndexOutOfBoundsException ex){
             app = "timvision.launcher";
        }
        return app;
   }

    public static ArrayList<String> getLog() {
        ArrayList<String> line = new ArrayList<>();

        String line1 = "";
        java.lang.Process p = null;
        BufferedReader in = null;
        int i=0;
        try {
            StringBuilder log=new StringBuilder();
            p = Runtime.getRuntime().exec("logcat *:W -d -t 5000");
            in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line1 = in.readLine()) != null) {

                    line.add(i, line1);

            }
        } catch (IOException e) {
            Log.e("adb logcat -d", "error in getting first line of top");
            e.printStackTrace();
        } finally {
            try {
                in.close();
                p.destroy();
            } catch (Exception e) {
                Log.e("adb logcat -d",
                        "error in closing and destroying top process");
                e.printStackTrace();
            }
        }

        return line;
    }


    private static float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" ");

            long idle1 = Long.parseLong(toks[5]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[4])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception e) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" ");

            long idle2 = Long.parseLong(toks[5]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[4])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    public static String getAvailableExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSizeLong();
            long availableBlocks = stat.getAvailableBlocksLong();
            return formatSize(availableBlocks * blockSize);
        } else {
            return "No SdCard";
        }
    }
    public static String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return formatSize(availableBlocks * blockSize);
    }

    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }


    public static boolean externalMemoryAvailable() {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        Boolean isSDSupportedDevice = Environment.isExternalStorageRemovable();

        if(isSDSupportedDevice && isSDPresent)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static ArrayList<Pinfo> getInstalledApps(boolean getSysPackages, Context context) {
        ArrayList<Pinfo> res = new ArrayList<Pinfo>();
        List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(0);
        for(int i=0;i<packs.size();i++) {
            PackageInfo p = packs.get(i);
            if ((!getSysPackages) && (p.versionName == null)) {
                continue ;
            }

            Pinfo newInfo = new Pinfo();
            newInfo.appname = p.applicationInfo.loadLabel(context.getPackageManager()).toString();
            newInfo.pname = p.packageName;
            newInfo.versionName = p.versionName;
            newInfo.versionCode = p.versionCode;
            newInfo.icon = p.applicationInfo.loadIcon(context.getPackageManager());
            newInfo.installDate = getDate(p.firstInstallTime,"dd/MM/yyyy")+"-"+getDate(p.lastUpdateTime,"dd/MM/yyyy");
            newInfo.running=isForeground(context, p.packageName);
            res.add(newInfo);


        }

        return res;
    }

    public static String screenSize(){
        String sysDisplaySize="0";
        try {
            Class<?> systemProperties = Class.forName("android.os.SystemProperties");
            Method getMethod = systemProperties.getMethod("get", String.class);
            sysDisplaySize = (String) getMethod.invoke(systemProperties, "sys.display-size");
        } catch (Exception e) {
            LogDM.LogE("Failed to read sys.display-size");
        }



        return sysDisplaySize;
    }


    public static int isForeground(Context ctx, String myPackage){
        ActivityManager manager = (ActivityManager) ctx.getSystemService(ACTIVITY_SERVICE);
        List< ActivityManager.RunningAppProcessInfo > runningTaskInfo = manager.getRunningAppProcesses();
        try {
            String a = runningTaskInfo.get(0).processName;
            if (a.equals(myPackage)) {
                return 1;
            }
            return 0;
        }catch (NullPointerException e){
            return 0;
        }
    }
    public static List<String> BluthootDevice(Context ctx){

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        List<String> s = new ArrayList<String>();
        for(BluetoothDevice bt : pairedDevices)
            if(bt.getName().contains("TIM")) {
                s.add(bt.getName() + "-" + getBatteryStatus(ctx)+" %");
            }else{
                s.add(bt.getName() + "-Non disponibile");
            }

        return s;
    }



    private static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getBatteryStatus(Context ctx){


        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        List<String> s = new ArrayList<String>();
        for(BluetoothDevice bt : pairedDevices) {
            if(bt.getName().contains("Telecomando")) {
                s.add(bt.getName());
                String livello_batteria = "rcu_battery_" + bt.getAddress().toString();
                return String.valueOf(Settings.Global.getInt(ctx.getContentResolver(), livello_batteria, -1));
            }
        }
        return "";

    }

    public static void downloadInstall(Context ctx, String apkurl){
        try {
            URL url = new URL(apkurl);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            String PATH = Environment.getExternalStorageDirectory() + "/Download/";
            File file = new File(PATH);
            file.mkdirs();
            File outputFile = new File(file, "app.apk");
            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }
            fos.close();
            is.close();



            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Download/" + "app.apk")), "application/vnd.android.package-archive");
            ctx.startActivity(intent);

        } catch (IOException e) {
            Toast.makeText(ctx.getApplicationContext(), "download error!", Toast.LENGTH_LONG).show();
        }
    }

    public static String upTime(){
        String uptime = "";

        upTime();

        return uptime;
        
    }
}
