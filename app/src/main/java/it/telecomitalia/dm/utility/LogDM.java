package it.telecomitalia.dm.utility;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import it.telecomitalia.dm.BuildConfig;

public class LogDM {

    public static void LogD(String stringLog){

        if(BuildConfig.DEBUG) {
            Log.d("DMAltran", stringLog);
        }

    }

    public static void LogD_Toast(Context context, String stringLog){

        if(BuildConfig.DEBUG) {
            Log.d("DMAltran", stringLog);
            Toast.makeText(context, stringLog, Toast.LENGTH_SHORT).show();
        }

    }


    public static void LogE(String stringLog){

        if(BuildConfig.DEBUG) {
            Log.e("DMAltran", stringLog);
        }

    }


}
