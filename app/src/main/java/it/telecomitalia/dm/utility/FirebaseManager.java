package it.telecomitalia.dm.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import it.telecomitalia.dm.PopUp;
import it.telecomitalia.dm.PopUp_Auth;
import it.telecomitalia.dm.models.STB;
import it.telecomitalia.dm.models.STB_Static;
//import com.timanager.mw.timanager.PopUp;
//import com.timanager.mw.timanager.SystemUtil.SystemInfo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by A on 18/10/2016.
 */

public class FirebaseManager {

    private DatabaseReference mPostReference;
    private DatabaseReference mDatabase;
    private static FirebaseManager ourInstance;
    private STB stb;
    private static Context ctx;


    public static FirebaseManager getInstance(String serial, Context context, Boolean isComunication) {
        if(ourInstance==null)
        {
            ctx = context;
            ourInstance = new FirebaseManager(serial, isComunication);

        }
        return ourInstance;

    }

    @SuppressLint("MissingPermission")
    private FirebaseManager(String stbSerial, Boolean isComunication) {
        if(!isComunication) {
            //User from JobService to send data programmatically
            mDatabase = FirebaseDatabase.getInstance().getReference().child("Device").child(stbSerial);
        } else {
            //Used to Service Screen ON / OFF to initialize Firebase Listner at Boot
            mDatabase = FirebaseDatabase.getInstance().getReference().child("Device").child(Build.getSerial());
            mPostReference = FirebaseDatabase.getInstance().getReference().child("Device").child(Build.getSerial()).child("static");
            ValueEventListener postListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // Get Post object and use the values to update the UI
                    STB_Static post = dataSnapshot.getValue(STB_Static.class);
                    if (post != null) {
                        if (post.getAction().compareTo("factory") == 0) {
                            Process proc = null;
                            try {
                                Toast.makeText(ctx, "Mi hanno chiesto di fatory", Toast.LENGTH_LONG).show();
                                String command;
                                command = "am broadcast -a android.intent.action.MASTER_CLEAR";
                                proc = Runtime.getRuntime().exec(command);
                                proc.waitFor();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (post.getAction().compareTo("reboot") == 0) {
                            Toast.makeText(ctx, "Mi hanno chiesto di essere riavviato", Toast.LENGTH_LONG).show();
                            SystemInfo.executeReboot(ctx);
                        }
                        if (post.getAction().contains("refresh")) {
                            Toast.makeText(ctx, "Mi hanno chiesto di modificare il tempo di collezionamento dei dati", Toast.LENGTH_LONG).show();
                            String action = post.getAction();
                            String timeMilliseconds = action.substring(action.indexOf("-") + 1, action.length());
                            if(Integer.parseInt(timeMilliseconds)<5000) {
                                SharedPreference.set_Int_Shared(ctx, Constants.KEY_ALL_INFOS, 5000);
                            }else{
                                SharedPreference.set_Int_Shared(ctx, Constants.KEY_ALL_INFOS, Integer.parseInt(timeMilliseconds));
                            }
                        }
                        if (post.getAction().contains("install")) {
                            String action = post.getAction();
                            String apkUrl = action.substring(action.indexOf("-") + 1, action.length());
                            //TestJobService te = new TestJobService();
                            new DownloadFileFromURL().execute(apkUrl);

                            Toast.makeText(ctx, "Mi hanno chiesto di installare un Applicazione", Toast.LENGTH_LONG).show();
                        }
                        if (post.getAction().contains("silent")) {
                            String action = post.getAction();
                            String apkUrl = action.substring(action.indexOf("-") + 1, action.length());
                            new DownloadFileFromURLSilent().execute(apkUrl);
                            Toast.makeText(ctx, "Mi hanno chiesto di installare un Applicazione", Toast.LENGTH_LONG).show();
                        }
                        if (post.getAction().contains("msg")) {
                            String action = post.getAction();
                            String msessaggio = action.substring(action.indexOf("-") + 1, action.length());
                            Intent intent1 = new Intent(ctx, PopUp.class);
                            intent1.putExtra("1", msessaggio);
                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ctx.startActivity(intent1);
                        }
                        if (post.getAction().compareTo("policy")==0) {
                            String action = post.getAction();

                            Intent intent1 = new Intent(ctx, PopUp_Auth.class);

                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ctx.startActivity(intent1);
                        }
                        if (post.getAction().contains("app")) {
                        }
                        if (post.getAction().contains("home")) {
                            SystemInfo.executeKeyCode(3);
                        }
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w("", "loadPost:onCancelled", databaseError.toException());
                }
            };
            mPostReference.addValueEventListener(postListener);
        }
    }
    public void salvaSTBValue(STB stb, String data, String ora){
        // Write a message to the database

        DatabaseReference myRef = mDatabase.child(data).child(ora);
        myRef.setValue(stb);


    }
    public void salvaSTBUptimeValue(String uptime, String data, String ora){
        // Write a message to the database

        DatabaseReference myRef = mDatabase.child(data).child("UpTime");
        myRef.setValue(uptime);


    }
    public void salvaSTB_action_NONE(){
        // Write a message to the database

        DatabaseReference myRef = mDatabase.child("static").child("action");
        myRef.setValue("none");


    }
    public void updateSTB_action(String value){
        // Write a message to the database

        DatabaseReference myRef = mDatabase.child("static").child("action");
        myRef.setValue(value);


    }
    public void salvaSTBStaticValue(STB_Static stb){
        // Write a message to the database

        DatabaseReference myRef = mDatabase;
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("static", stb);
        myRef.updateChildren(childUpdates);



    }
    public STB getSTBValues(){
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {
                    stb =(child.getValue(STB.class));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("errore pppppppp");
            }

        });
        return stb;
    }
    public class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().toString()
                        + "/Download/app.apk");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    System.out.println("OTA Download Data starting");
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            /*Intent install = new Intent(Intent.ACTION_VIEW);
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            install.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Download/app.apk")), "application/vnd.android.package-archive");
            ctx.startActivity(install);*/

            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri fileUri = FileProvider.getUriForFile(ctx, "it.telecomitalia.dm.provider", new File("storage/emulated/0/Download/app.apk"));
            intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            ctx.startActivity(intent);

            /*Intent intent1 = new Intent(ctx, PopUp.class);
            intent1.putExtra("1", "installo");
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(intent1);*/


        }

    }
    class DownloadFileFromURLSilent extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();



        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().toString()
                        + "/Download/app.apk");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    System.out.println("OTA Download Data starting");
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            /*Intent install = new Intent(Intent.ACTION_VIEW);
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            install.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Download/app.apk")), "application/vnd.android.package-archive");
            startActivity(install);
*/
            File file = new File(Environment.getExternalStorageDirectory() + "/Download/app.apk");
            if(file.exists()){
                Process proc=null;
                try {
                    Log.d("DM", "inizio installazione");
                    String command;
                    command = "pm install -r " + "/storage/emulated/0/Download/app.apk";
                    proc = Runtime.getRuntime().exec(command);
                    Log.d("DM", "inizio eseguito il comnado "+ command);
                    proc.waitFor();
                } catch (Exception e) {
                    Log.d("DM", "eccezione " + e.toString() );
                    e.printStackTrace();
                }finally {
                    proc.destroy();
                }
            }




        }

    }
}
