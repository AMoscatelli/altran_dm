package it.telecomitalia.dm.utility;

public class Constants {

    public static int REFRESH_ALL_INFO = 5000;
    public static int REFRESH_ALL_INFO_STANDBY = 600000;
    public static String SERIAL_NUMBER = "ro.serialno";
    public static String DEVICE_NAME = "ro.product.board";
    public static String KEY_ALL_INFOS = "all_info";


}
