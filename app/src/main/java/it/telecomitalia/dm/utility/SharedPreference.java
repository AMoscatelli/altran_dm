package it.telecomitalia.dm.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import it.telecomitalia.dm.R;

public class SharedPreference {

    public static void set_Int_Shared(Context context, String key, int value){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();

    }

    public static int get_Int_Shared(Context context, String keyShared, int default_value){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getInt(keyShared, default_value);

    }

}
