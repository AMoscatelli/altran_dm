package it.telecomitalia.dm.models;

import java.io.Serializable;

/**
 * Created by A on 17/10/2016.
 */

public class STB implements Serializable {

    private String CPUUsage;
    private String RAMAvailable;
    private String WIFISignal;
    private String storageAvailable_EXT;
    private String storageAvailable_INT;
    private String temperature;
    private String RCUBattery;
    private String GPBattery;
    private String upTime;




    public STB(String CPUUsage, String RAMAvailable, String WIFISignal, String storageAvailable_EXT,
               String storageAvailable_INT, String temperature, String RCUBattery , String GPBattery , String upTime){

        this.CPUUsage = CPUUsage;
        this.RAMAvailable = RAMAvailable;
        this.WIFISignal = WIFISignal;
        this.storageAvailable_EXT = storageAvailable_EXT;
        this.storageAvailable_INT = storageAvailable_INT;
        this.temperature = temperature;
        this.RCUBattery = RCUBattery;
        this.upTime = upTime;
        this.GPBattery = GPBattery;
    }

    public STB(){
    }

    public String getGPBattery() {
        return GPBattery;
    }

    public void setGPBattery(String GPBattery) {
        this.GPBattery = GPBattery;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public String getCPUUsage() {
        return CPUUsage;
    }

    public void setCPUUsage(String CPUUsage) {
        this.CPUUsage = CPUUsage;
    }

    public String getRAMAvailable() {
        return RAMAvailable;
    }

    public void setRAMAvailable(String RAMAvailable) {
        this.RAMAvailable = RAMAvailable;
    }

    public String getWIFISignal() {
        return WIFISignal;
    }

    public void setWIFISignal(String WIFISignal) {
        this.WIFISignal = WIFISignal;
    }

    public String getStorageAvailable_EXT() {
        return storageAvailable_EXT;
    }

    public void setStorageAvailable_EXT(String storageAvailable_EXT) {
        this.storageAvailable_EXT = storageAvailable_EXT;
    }

    public String getStorageAvailable_INT() {
        return storageAvailable_INT;
    }

    public void setStorageAvailable_INT(String storageAvailable_INT) {
        this.storageAvailable_INT = storageAvailable_INT;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getRCUBattery() {
        return RCUBattery;
    }

    public void setRCUBattery(String RCUBattery) {
        this.RCUBattery = RCUBattery;
    }
}
