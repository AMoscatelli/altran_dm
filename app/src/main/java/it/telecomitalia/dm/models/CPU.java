package it.telecomitalia.dm.models;

import java.io.Serializable;

/**
 * Created by A on 17/10/2016.
 */

public class CPU implements Serializable {

    private int core;
    private long CPUActive;
    private long CPUTotal;
    private int CPULoadPercentuale;


    public CPU(int core, long CPUActive, long CPUTotal, int CPULoadPercentuale){

        this.core = core;
        this.CPUActive = CPUActive;
        this.CPUTotal = CPUTotal;
        this.CPULoadPercentuale = CPULoadPercentuale;
    }

    public CPU(){
    }

    public int getCore() {
        return core;
    }

    public void setCore(int core) {
        this.core = core;
    }

    public long getCPUActive() {
        return CPUActive;
    }

    public void setCPUActive(long CPUActive) {
        this.CPUActive = CPUActive;
    }

    public long getCPUTotal() {
        return CPUTotal;
    }

    public void setCPUTotal(long CPUTotal) {
        this.CPUTotal = CPUTotal;
    }

    public int getCPULoadPercentuale() {
        return CPULoadPercentuale;
    }

    public void setCPULoadPercentuale(int CPULoadPercentuale) {
        this.CPULoadPercentuale = CPULoadPercentuale;
    }
}
