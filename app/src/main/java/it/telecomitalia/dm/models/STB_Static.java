package it.telecomitalia.dm.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by A on 17/10/2016.
 */

public class STB_Static implements Serializable {


    private String deviceFingerPrint;
    private String deviceModel;
    private String deviceOS;
    private String deviceRooted;
    private String deviceUpTime;
    private String connectionType;
    private String status;
    private String position;
    private String action;
    private String memInt;
    private String memExt;
    private String wifiSignal;
    private String screenSize;
    private String gmailAccountPresent;
    private List<String> application;
    private List<String> BTDevice;
    private List<String> Log;




    public STB_Static(String deviceFingerPrint, String deviceModel, String deviceOS, String deviceRooted, String deviceUpTime,
                      String connectionType, String status, String position, String action, String memInt, String memExt, String wifiSignal, String screenSize, List<String> application, List<String> BTDevice, List<String> Log, String gmailAccountPresent){

        this.deviceFingerPrint = deviceFingerPrint;
        this.deviceModel = deviceModel;
        this.deviceOS = deviceOS;
        this.deviceRooted = deviceRooted;
        this.deviceUpTime = deviceUpTime;
        this.connectionType = connectionType;
        this.status = status;
        this.position = position;
        this.action = action;
        this.memInt = memInt;
        this.memExt = memExt;
        this.wifiSignal = wifiSignal;
        this.screenSize = screenSize;
        this.application = application;
        this.BTDevice = BTDevice;
        this.Log = Log;
        this.gmailAccountPresent = gmailAccountPresent;
    }

    public STB_Static(){
    }

    public String getGmailAccountPresent() {
        return gmailAccountPresent;
    }

    public void setGmailAccountPresent(String gmailAccountPresent) {
        this.gmailAccountPresent = gmailAccountPresent;
    }

    public String getDeviceFingerPrint() {
        return deviceFingerPrint;
    }

    public void setDeviceFingerPrint(String deviceFingerPrint) {
        this.deviceFingerPrint = deviceFingerPrint;
    }

    public List<String> getLog() {
        return Log;
    }

    public void setLog(List<String> log) {
        Log = log;
    }

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        this.screenSize = screenSize;
    }

    public String getMemInt() {
        return memInt;
    }

    public String getWifiSignal() {
        return wifiSignal;
    }

    public void setWifiSignal(String wifiSignal) {
        this.wifiSignal = wifiSignal;
    }

    public void setMemInt(String memInt) {
        this.memInt = memInt;
    }

    public String getMemExt() {
        return memExt;
    }

    public void setMemExt(String memExt) {
        this.memExt = memExt;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getDeviceRooted() {
        return deviceRooted;
    }

    public void setDeviceRooted(String deviceRooted) {
        this.deviceRooted = deviceRooted;
    }

    public String getDeviceUpTime() {
        return deviceUpTime;
    }

    public void setDeviceUpTime(String deviceUpTime) {
        this.deviceUpTime = deviceUpTime;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<String> getApplication() {
        return application;
    }

    public void setApplication(List<String> application) {
        this.application = application;
    }

    public List<String> getBTDevice() {
        return BTDevice;
    }

    public void setBTDevice(List<String> BTDevice) {
        this.BTDevice = BTDevice;
    }
}
