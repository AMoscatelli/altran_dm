package it.telecomitalia.dm.models;

import android.graphics.drawable.Drawable;

/**
 * Created by MW on 11/10/2017.
 */

public class Pinfo {
    public String appname = "";
    public String pname = "";
    public String versionName = "";
    public int versionCode = 0;
    public Drawable icon;
    public String installDate = "";
    public int running = 0;

}