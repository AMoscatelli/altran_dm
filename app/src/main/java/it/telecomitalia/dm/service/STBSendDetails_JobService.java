package it.telecomitalia.dm.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.CpuUsageInfo;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.Log;

import com.technicolor.hardwareproperties.IHardwarePropertiesService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import it.telecomitalia.dm.models.CPU;
import it.telecomitalia.dm.models.Pinfo;
import it.telecomitalia.dm.models.STB;
import it.telecomitalia.dm.models.STB_Static;
import it.telecomitalia.dm.utility.FirebaseManager;
import it.telecomitalia.dm.utility.LogDM;
import it.telecomitalia.dm.utility.SharedPreference;
import it.telecomitalia.dm.utility.SystemInfo;

/**
 * JobService to be scheduled by the JobScheduler.
 * start another service
 */
public class STBSendDetails_JobService extends JobService {

    private Context ctx;
    private FirebaseManager fManager;



    @Override
    public boolean onStartJob(JobParameters params) {
        LogDM.LogD("on start Job - execution");
        startService();
        LogDM.LogD("Reschedule service");
        ServiceUtil.scheduleJobDetails(getApplicationContext()); // reschedule the job
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    private void startService() {
        LogDM.LogD("Job - start service");
        ctx = this;
            // TODO: 20/09/2019 inserire il controllo per far partire il main task sulla base del tempo di refresh dello static
        LogDM.LogD("task all info");
            // TODO: 19/09/2019 rimuovere il deprecato e sostituire con  SystemInfo.getSystemProperties(Constants.SERIAL_NUMBER)
        fManager = FirebaseManager.getInstance(Build.SERIAL, getApplicationContext(), false);
        try {
            mainTask();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    private void mainTask() throws RemoteException {

        //System.out.println(Build.SERIAL);
        LogDM.LogD("controllo foreground");
        LogDM.LogD(SystemInfo.executeTopApplication());
        Date currentTime = Calendar.getInstance().getTime();

        //final String durationBreakdown = getDurationBreakdown(SystemClock.uptimeMillis(), true);
        String dataLastLogin = (String) DateFormat.format("dd/MM/yyyy", currentTime);
        String oraLastLogin = (String) DateFormat.format("kk:mm", currentTime);
        String data = (String) DateFormat.format("ddMMyyyy", currentTime);
        String ora = (String) DateFormat.format("kkmm", currentTime);
        String oranodo = ora.substring(0, 3) + "0";
        final String ram = SystemInfo.readFreeMem(ctx);
        final ArrayList<Pinfo> installedApps = SystemInfo.getInstalledApps(false, getApplicationContext());
        final List<String> app = new ArrayList<>();
        final String wifiSignal = (SystemInfo.getWifiSignal(ctx));


        STB stb = new STB();

        stb.setRAMAvailable(ram);
        stb.setWIFISignal(wifiSignal);
        stb.setStorageAvailable_EXT(SystemInfo.getAvailableExternalMemorySize());
        stb.setStorageAvailable_INT(SystemInfo.getAvailableInternalMemorySize());
        stb.setTemperature("0");
        stb.setCPUUsage("0");

        stb.setRCUBattery(SystemInfo.getBatteryStatus(ctx));
        stb.setUpTime("0" + " - " + dataLastLogin + " - " + oraLastLogin);

        ctx.stopService(new Intent(this, CpuService.class));
        //Scrivo i valori nel DB

        fManager.salvaSTBValue(stb, data, oranodo);

        fManager.salvaSTBUptimeValue(String.valueOf(0), data, oranodo);


        int j = 0;
        for (int i = 0; i < installedApps.size(); i++) {
            if (installedApps.get(i).pname.contains("telecom") ||
                    installedApps.get(i).pname.contains("gameloft") ||
                    installedApps.get(i).pname.contains("premium") ||
                    installedApps.get(i).pname.contains("youtube") ||
                    installedApps.get(i).pname.contains("tim") ||
                    installedApps.get(i).pname.contains("dtvinput") ||
                    installedApps.get(i).pname.contains("sensiunici") ||
                    installedApps.get(i).pname.contains("netflix")) {
                Log.d("DM FG", SystemInfo.executeTopApplication());
                Log.d("DM APP", installedApps.get(i).pname);
                if (SystemInfo.executeTopApplication().trim().compareTo(installedApps.get(i).pname.trim()) == 0) {
                    Log.d("DM attivo", installedApps.get(i).pname);
                    app.add(j, installedApps.get(i).appname + " | " + installedApps.get(i).versionName + " | " + installedApps.get(i).installDate + " | " + 1 + " | " + installedApps.get(i).versionCode + " | " + installedApps.get(i).pname);

                } else {
                    Log.d("DM non attivo", installedApps.get(i).pname);
                    app.add(j, installedApps.get(i).appname + " | " + installedApps.get(i).versionName + " | " + installedApps.get(i).installDate + " | " + 0 + " | " + installedApps.get(i).versionCode + " | " + installedApps.get(i).pname);

                }
                j++;
            }
        }


        STB_Static stb_static = new STB_Static();

        stb_static.setDeviceFingerPrint(Build.FINGERPRINT);
        stb_static.setDeviceModel(Build.MANUFACTURER + " - " + Build.MODEL + " - " + Build.DEVICE);
        stb_static.setDeviceOS(Build.VERSION.RELEASE);
        stb_static.setDeviceRooted("NO");
        //stb_static.setDeviceUpTime("0" + "-" + dataLastLogin + " - " + oraLastLogin);
        stb_static.setDeviceUpTime(String.valueOf(SystemClock.uptimeMillis()));
        stb_static.setConnectionType(SystemInfo.isWifiConnected(ctx) ? "Wifi" : "ETH");
        stb_static.setStatus(data + "-" + ora);
        stb_static.setPosition(12.4686364 + "/" + 41.8972886);
        stb_static.setAction("none");
        stb_static.setApplication(app);
        stb_static.setBTDevice(SystemInfo.BluthootDevice(getApplicationContext()));
        stb_static.setMemInt(SystemInfo.getAvailableInternalMemorySize());
        stb_static.setMemExt(SystemInfo.getAvailableExternalMemorySize());
        stb_static.setWifiSignal(wifiSignal);
        stb_static.setScreenSize(SystemInfo.screenSize());
        stb_static.setLog(SystemInfo.getLog());
        stb_static.setGmailAccountPresent(SystemInfo.googleAccountPresent(ctx)?"1":"0");
        System.out.println("service started");
        //Update Static Values
        fManager.salvaSTBStaticValue(stb_static);


    }


}
