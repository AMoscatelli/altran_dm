package it.telecomitalia.dm.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.icu.text.RelativeDateTimeFormatter;
import android.os.Binder;
import android.os.CpuUsageInfo;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.technicolor.hardwareproperties.IHardwarePropertiesService;

import java.util.ArrayList;

import it.telecomitalia.dm.models.CPU;
import it.telecomitalia.dm.utility.SharedPreference;

public class CpuService extends Service {
    public CpuService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }


    //Begin Code from TCH
    private static final String READ_HARDWARE_PROPERTY_ACTION = "com.technicolor.hardwareproperties.READ_HARDWARE_PROPERTY";
    private static final String READ_HARDWARE_PROPERTY_PACKAGE_NAME = "com.technicolor.hardwareproperties";
    private static final String TAG = "HardwarePropertiesTest";
    private static IHardwarePropertiesService mService;


    public ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, "onServiceConnected");
            try {
                mService = IHardwarePropertiesService.Stub.asInterface(binder);
                SharedPreference.set_Int_Shared(getApplicationContext(),"temp", geCpuTemperature());
                SharedPreference.set_Int_Shared(getApplicationContext(),"cpu", getCpuUsages());

                //testReadCpuTemperature();
                //testReadCpuUsages();
                unbindService(this);
            } catch (Exception e) {
                Log.e(TAG, "onServiceConnected", e);
            }
        }
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, "onServiceDisconnected");
            mService = null;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        String CHANNEL_ID = "your_channel_id";
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                "Notification Channel Title",
                NotificationManager.IMPORTANCE_DEFAULT);

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("")
                .setContentText("").build();

        startForeground(1, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        String CHANNEL_ID = "your_channel_id";
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                "Notification Channel Title",
                NotificationManager.IMPORTANCE_DEFAULT);

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("")
                .setContentText("").build();

        startForeground(1, notification);
        if (intent!=null){
            try {
                intent = new Intent(READ_HARDWARE_PROPERTY_ACTION);
                intent.setPackage(READ_HARDWARE_PROPERTY_PACKAGE_NAME);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            } catch (Exception e) {
                Log.e(TAG, "onHandleIntent()", e);
            }
        }
        return START_STICKY;
    }

    public  int geCpuTemperature() throws RemoteException {
        Float cpuTemperature = mService.getCpuTemperature();
        Log.i(TAG, "CPU temperature:" + cpuTemperature);
        return cpuTemperature.intValue();
    }

    public  int getCpuUsages() throws RemoteException {
        ArrayList<CPU> cpuList = new ArrayList<>();
        CpuUsageInfo[] cpuUsages = mService.getCpuUsages();
        int cpuAverageLoad=0;
        if (cpuUsages!=null){
            for (int i= 0; i<cpuUsages.length; i++){
                CPU cpu = new CPU();
                cpu.setCore(i+1);
                cpu.setCPUActive(cpuUsages[i].getActive());
                cpu.setCPUTotal(cpuUsages[i].getTotal());
                Long percento = cpuUsages[i].getActive()/cpuUsages[i].getTotal()*100;
                cpu.setCPULoadPercentuale(percento.intValue());
                Log.d(TAG, "Read Cpu Usage: core-"+i+": activeTime="+cpuUsages[i].getActive()+", totalTime="+cpuUsages[i].getTotal());
                cpuList.add(cpu);
            }

            for (int i= 0; i<cpuUsages.length; i++){
                cpuAverageLoad = cpuAverageLoad + cpuList.get(i).getCPULoadPercentuale();
            }
        }
        return cpuAverageLoad/4;
    }
    // End code from TCH
}
