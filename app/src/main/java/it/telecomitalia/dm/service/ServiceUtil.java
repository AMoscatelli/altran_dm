package it.telecomitalia.dm.service;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;

import it.telecomitalia.dm.utility.Constants;
import it.telecomitalia.dm.utility.LogDM;
import it.telecomitalia.dm.utility.SharedPreference;

public class ServiceUtil {

    public static void scheduleJobDetails(Context context) {
        LogDM.LogD("Schedule Job Details launching");
            LogDM.LogD("refresh infos standard");
            ComponentName serviceComponent = new ComponentName(context, STBSendDetails_JobService.class);
            JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
            builder.setMinimumLatency(1 * SharedPreference.get_Int_Shared(context, Constants.KEY_ALL_INFOS, Constants.REFRESH_ALL_INFO) ); // wait at least
            builder.setOverrideDeadline(3 * SharedPreference.get_Int_Shared(context, Constants.KEY_ALL_INFOS, Constants.REFRESH_ALL_INFO) ); // maximum delay
            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
            jobScheduler.schedule(builder.build());

    }


}
