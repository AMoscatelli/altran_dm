package it.telecomitalia.dm.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.CpuUsageInfo;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.ArrayMap;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import com.technicolor.hardwareproperties.IHardwarePropertiesService;

import it.telecomitalia.dm.models.CPU;
import it.telecomitalia.dm.models.Pinfo;
import it.telecomitalia.dm.utility.Constants;
import it.telecomitalia.dm.utility.FirebaseManager;
import it.telecomitalia.dm.utility.LogDM;
import it.telecomitalia.dm.utility.SharedPreference;
import it.telecomitalia.dm.utility.SystemInfo;


public class ServiceScreenON_OFF extends Service {

    private ScreenBroadcastReceiver m_receiver;
    private FirebaseManager fManager;
    private RequestQueue mRequestQueue;
    private boolean on = true;




    public ServiceScreenON_OFF() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        m_receiver = new ScreenBroadcastReceiver();
        registerReceiver(m_receiver, filter);

        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "your_channel_id";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Notification Channel Title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }
        fManager = FirebaseManager.getInstance(Build.SERIAL, this, true);
    }

    private class ScreenBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                LogDM.LogD( "SCREEN ON - controllo delle app aggiornate");
                SharedPreference.set_Int_Shared(context, Constants.KEY_ALL_INFOS,Constants.REFRESH_ALL_INFO);
                checkAppUpdate(context);


            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                LogDM.LogD( "SCREEN OFF - controllo delle app aggiornate");
                SharedPreference.set_Int_Shared(context, Constants.KEY_ALL_INFOS,Constants.REFRESH_ALL_INFO_STANDBY);
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(m_receiver);
    }

    private void checkAppUpdate(Context context) {
        // TODO: 19/09/2019 Rimuovere il deprecato SERIAL usando SystemInfo.getSystemProperties(Constants.SERIAL_NUMBER)
        if (Build.SERIAL.compareTo("4010544900000071") == 0 || Build.SERIAL.compareTo("4010544900000076") == 0 || Build.SERIAL.compareTo("4010544900000405") == 0) {
            final ArrayList<Pinfo> installedApps = SystemInfo.getInstalledApps(false, context);

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            Log.d("ON SCREEN ON", preferences.getBoolean("check", true) ? "TRUE" : "FALSE");

            if (preferences.getBoolean("check", true)) {
                for (int i = 0; i < installedApps.size(); i++) {

                    String name = preferences.getString(installedApps.get(i).pname, "");
                    if (!name.equalsIgnoreCase("")) {
                        if (name.compareTo(installedApps.get(i).installDate) == 0) {
                            //Toast.makeText(ctx, installedApps.get(i).pname+" non è stata aggiornata", Toast.LENGTH_SHORT).show();
                            Log.d("CONTROLLO APP", installedApps.get(i).pname + " non è stata aggiornata");
                        } else {
                            //Toast.makeText(ctx, installedApps.get(i).pname+" è stata aggiornata!!!!!", Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(installedApps.get(i).pname, installedApps.get(i).installDate);
                            editor.apply();
                            Log.d("CONTROLLO APP", installedApps.get(i).pname + " è stata aggiornata!!!!!");

                            // the request queue
                            mRequestQueue = Volley.newRequestQueue(context);

                            HashMap<String, String> map = new HashMap<>();
                            map.put("package", installedApps.get(i).pname);
                            map.put("versione", installedApps.get(i).versionName);
                            // TODO: 19/09/2019 Rimuovere il deprecato SERIAL e sostituire con SystemInfo.getSystemProperties(Constants.SERIAL_NUMBER)
                            map.put("serial", Build.SERIAL);
                            map.put("fw", Build.MANUFACTURER + " - " + Build.MODEL + " - " + Build.DEVICE);
                            JSONObject pippo = new JSONObject(map);
                            Log.d("JSON SCREEN ON", pippo.toString());

                            JsonObjectRequest request = new JsonObjectRequest(
                                    Request.Method.POST, // the request method
                                    "http://www.moscatelliweb.idb t/test.php", // the URL
                                    new JSONObject(map), // the parameters for the php
                                    new Response.Listener<JSONObject>() { // the response listener
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.d("ON SCREEN ON", "mail inviata");
                                        }
                                    },
                                    new Response.ErrorListener() { // the error listener
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.d("ON SCREEN ON", "php error - " + error.toString());
                                        }
                                    });
                            mRequestQueue.add(request);
                        }
                    } else {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(installedApps.get(i).pname, installedApps.get(i).installDate);
                        editor.apply();
                        Log.d("CONTROLLO APP", installedApps.get(i).pname + " è stata aggiunta!!!!!");
                    }
                }
            }
            Log.d("ON SCREEN ON", "fine controllo delle app aggiornate");
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("Check", false);
            Log.d("ON SCREEN ON", "CHECK FALSE");
            editor.apply();
            on = true;
        }
    }
}
