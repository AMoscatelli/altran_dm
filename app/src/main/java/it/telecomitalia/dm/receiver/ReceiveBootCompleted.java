package it.telecomitalia.dm.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import it.telecomitalia.dm.BuildConfig;
import it.telecomitalia.dm.service.ServiceScreenON_OFF;
import it.telecomitalia.dm.service.ServiceUtil;
import it.telecomitalia.dm.utility.Constants;
import it.telecomitalia.dm.utility.LogDM;
import it.telecomitalia.dm.utility.SharedPreference;
import it.telecomitalia.dm.utility.SystemInfo;



public class ReceiveBootCompleted extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            if(BuildConfig.DEBUG) {
                Toast.makeText(context, "Start Service DM", Toast.LENGTH_SHORT).show();
                LogDM.LogD_Toast(context, "Catched boot completed");
            }
            //TODO restizione di Telecom ATTIVA per far funzionare il DM solo su TIMBOX mediante check della chiave ro.product.board
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && SystemInfo.getSystemProperties(Constants.DEVICE_NAME).compareTo("skipper_tim")==0) {
                LogDM.LogD("Start service");
                context.startForegroundService(new Intent(context, ServiceScreenON_OFF.class));
                SharedPreference.set_Int_Shared(context, Constants.KEY_ALL_INFOS, Constants.REFRESH_ALL_INFO);
                ServiceUtil.scheduleJobDetails(context);
            /*}else{
                LogDM.LogD("Altran DM cannot start on release less than Android O o No TIM BOMX ");
                Toast.makeText(context, "Altran DM cannot start on release less than Android O", Toast.LENGTH_SHORT).show();
            }*/

        }

    }

}
