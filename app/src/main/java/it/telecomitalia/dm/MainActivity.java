package it.telecomitalia.dm;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuthException;


import java.util.Properties;

import it.telecomitalia.dm.models.CPU;
import it.telecomitalia.dm.service.CpuService;
import it.telecomitalia.dm.service.ServiceScreenON_OFF;
import it.telecomitalia.dm.service.ServiceUtil;
import it.telecomitalia.dm.utility.Constants;
import it.telecomitalia.dm.utility.FirebaseManager;
import it.telecomitalia.dm.utility.SharedPreference;

public class MainActivity extends Activity {
    private FirebaseManager fManager;
    private TextView rss;

    public static String KAFKA_BROKERS = "localhost:9092";
    public static Integer MESSAGE_COUNT=1000;
    public static String CLIENT_ID="client1";
    public static String TOPIC_NAME="demo";
    public static String GROUP_ID_CONFIG="consumerGroup1";
    public static Integer MAX_NO_MESSAGE_FOUND_COUNT=100;
    public static String OFFSET_RESET_LATEST="latest";
    public static String OFFSET_RESET_EARLIER="earliest";
    public static Integer MAX_POLL_RECORDS=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        Intent intent1 = new Intent(this, PopUp_Auth.class);
//
//        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        this.startActivity(intent1);

        CPU cpu  = new CPU();
        cpu.setCore(1);


//        this.startForegroundService(new Intent(this, CpuService.class));

    }

}
