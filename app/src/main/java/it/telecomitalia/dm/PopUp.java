package it.telecomitalia.dm;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

public class PopUp extends Activity {

    private TextView msg;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up);
        this.setTitle("TIM - Device Management");
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        msg = (TextView) findViewById(R.id.msg);
        button = (Button) findViewById(R.id.butOK);
        String s = getIntent().getStringExtra("1");
        if(s.compareTo("installo")!=0) {
            msg.setText(s);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    close();
                }
            });

            cunCountDownTimer.start();
        }else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), "it.telecomitalia.dm.provider", new File("storage/emulated/0/Download/app.apk"));
            intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        }
    }
    private void close(){
        this.finish();
    }
       CountDownTimer cunCountDownTimer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                // TODO Auto-generated method stub
                String sec = String.valueOf(millisUntilFinished);
                sec = sec.substring(0,1);
                button.setText("OK ("+sec+")");

            }

            @Override
            public void onFinish() {
                // TODO Auto-generated method stub
                close();
            }
        }.start();
    }
