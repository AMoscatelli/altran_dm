package it.telecomitalia.dm;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

import it.telecomitalia.dm.utility.FirebaseManager;

public class PopUp_Auth extends Activity {

    private TextView msg;
    private Button button;
    private FirebaseManager fManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_auth);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        button = (Button) findViewById(R.id.butOK);
        fManager = FirebaseManager.getInstance(Build.SERIAL, getApplicationContext(), false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fManager.updateSTB_action("OK");
                close();
            }
        });

            cunCountDownTimer.start();

    }
    private void close(){
        this.finish();
    }
       CountDownTimer cunCountDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                // TODO Auto-generated method stub
                String sec = String.valueOf(millisUntilFinished);
                if(millisUntilFinished > 9999)
                    sec = sec.substring(0,2);
                else
                    sec = sec.substring(0,1);
                button.setText("OK ("+sec+")");

            }

            @Override
            public void onFinish() {
                // TODO Auto-generated method stub
                fManager.updateSTB_action("KO");
                close();
            }
        }.start();
    }
